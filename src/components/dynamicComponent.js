import React, { PropTypes } from 'react';
import { Form, Select, Button, Radio, Row, Col, Input, InputNumber, DatePicker, Upload, Icon, message } from 'antd';
import { Link } from 'dva/router';
import moment from 'moment';

const RadioGroup = Radio.Group;
const Option = Select.Option;
const FormItem = Form.Item;
const ButtonGroup = Button.Group;

let uuid = 0;
class DynamicComponent extends React.Component {
  constructor(props) {
    super(props);
    const options = this.props.options;
    const data = this.props.data || {};
    const readOnly=this.props.readOnly || false;
    this.state = {};
    this.state[options.id] = data[options.id];

    if (!this.state[options.id]) {
            // set init value
      if (options.type == 'inputList') {
        const row = { id: uuid };
        options.columns.map((col) => {
          row[col.name] = '';
        });
        this.state[options.id] = [row];
      }
    }
  }

  handleFieldChange = (field, value, rIndex, col) => {
    console.log(`handleFieldChange id=${field} value=${value} rIndex=${rIndex} col=${col}`);
    const onChange = this.props.onChange;
    const options = this.props.options;
    const dateFormat = 'YYYY/MM/DD';
    if (options.type === 'date') {
      const fieldObject = {};
      //convert momoent to string
      value=moment(value).format(dateFormat);
      fieldObject[field] = value;
      this.setState(fieldObject);
      if (onChange) {
        onChange(Object.assign({}, this.state, fieldObject));
      }
    } else if (options.type === 'inputList') {
      const rows = this.state[field];
      rows[rIndex][col] = value;
      const fieldObject = {};
      fieldObject[field] = rows;
      this.setState(fieldObject);
      if (onChange) {
        onChange(Object.assign({}, this.state, fieldObject));
      }
    } else {
      const fieldObject = {};
      fieldObject[field] = value;
      this.setState(fieldObject);
      if (onChange) {
        onChange(Object.assign({}, this.state, fieldObject));
      }
    }
  }

  remove = (rIndex) => {
    const options = this.props.options;
    const rows = this.state[options.id];
    rows.splice(rIndex, 1);
    const fieldObject = {};
    fieldObject[options.id] = rows;
    this.setState(fieldObject);
  }

  add = () => {
    uuid++;
    const options = this.props.options;
    const rows = this.state[options.id];
    const row = { id: uuid };
    options.columns.map((col) => {
      row[col.name] = '';
    });
    rows.push(row);
    const fieldObject = {};
    fieldObject[options.id] = rows;
    this.setState(fieldObject);
  }

  render() {
    const state = this.state;
    const options = this.props.options;
    const field = options.id;
    const readOnly=this.props.readOnly || false;
    const data = this.props.data || {};
    const dateFormat = 'YYYY/MM/DD';

    if (options.type === 'text') {
      return <span key={options.key}> {options.value}</span>;
    }
    if (options.type === 'input') {
      return (readOnly?<span style={{ fontWeight: 'bold'}}>{data[field]}</span>:
      <Input
        id={field} key={field} type="text" size="default"
        value={state[field]} style={{ width: '20%', marginRight: '5px' }}
        onChange={(e) => {
          const value = e.target.value;
          this.handleFieldChange(field, value);
        }
            }
      />);
    }
    if (options.type === 'select') {
      let dataValue='';
      const selectOptions = options.options.map((option) => {
          if(option.key==data[field])
            dataValue=option.value;
	    	  return (<Option key={option.key} value={String(option.key)}>{option.value}</Option>);
		    });
      return (readOnly?<span style={{ fontWeight: 'bold'}}>{dataValue}</span>:<Select
        id={field}
        value={state[field]}
        size="default"
        style={{ width: '20%' }}
        onChange={(...arg) => {
          this.handleFieldChange(field, ...arg);
        }
           }
      >
        {selectOptions}
      </Select>);
    }
    if (options.type === 'radio') {
      let dataValue='';
      const radioOptions = options.options.map((option) => {
        if(option.key==data[field])
            dataValue=option.value;
	    	  return (<Radio key={option.key} value={option.key}>{option.value}</Radio>);
		    });
      return (readOnly?<span style={{ fontWeight: 'bold'}}>{dataValue}</span>:<RadioGroup
        onChange={(e) => { this.handleFieldChange(field, e.target.value); }}
        value={state[field]}
      >
        {radioOptions}
      </RadioGroup>);
    }
    if (options.type === 'date') {
      return (readOnly?<span style={{ fontWeight: 'bold'}}>{data[field]}</span>:<DatePicker
        size="default" format={dateFormat}
        onChange={(...arg) => {
          this.handleFieldChange(field, ...arg);
        }
                }
        value={state[field]?moment(state[field],dateFormat):moment()} 
      />);
    }
    if (options.type === 'file') {
      const props = {
        name: 'file',
        action: '//jsonplaceholder.typicode.com/posts/',
        headers: {
          authorization: 'authorization-text',
        },
        onChange(info) {
          if (info.file.status !== 'uploading') {
            console.log(info.file, info.fileList);
          }
          if (info.file.status === 'done') {
            message.success(`${info.file.name} file uploaded successfully`);
          } else if (info.file.status === 'error') {
            message.error(`${info.file.name} file upload failed.`);
          }
        },
      };
      return (readOnly?<span style={{ fontWeight: 'bold'}}>{data[field]}</span>:<Upload {...props}>
        <Button>
          <Icon type="upload" /> Click to Upload
                        </Button>
      </Upload>);
    }
    if (options.type === 'inputList') {
      const colSpan = 24 / (2 + options.columns.length);
            /* const cols=options.columns.map((col) => {
	    	  return (<Col key={col.name} span={colSpan}>
                <Input id={col.name} key={col.name} type="text" size="default" placeholder={col.name}
                    value={state[field][0]['name']} style={{ width: '50%', marginRight: '5px' }}
                    onChange={(e)=> {
                            let value=e.target.value;
                            this.handleFieldChange(field,value);
                        }
                    }/>
              </Col>)
		    });*/
      const rows = state[field].map((r, rIndex) => {
        return (readOnly?<span >{state[field]}</span>:<Row key={rIndex} style={{ width: '100%' }}>
          <Col span="2">{rIndex + 1}</Col>
          {
                            options.columns.map((col) => {
                              return (<Col key={col.name} span={colSpan}>
                                <Input
                                  id={col.name} key={col.name} type="text" size="default" placeholder={col.name}
                                  value={state[field][rIndex][col.name]} style={{ width: '50%', marginRight: '5px' }}
                                  onChange={(e) => {
                                    const value = e.target.value;
                                    this.handleFieldChange(field, value, rIndex, col.name);
                                  }
                                    }
                                />
                              </Col>);
                            })
                        }
          <Col span="8">
            <Icon
              className="dynamic-delete-button"
              type="minus-circle-o"
              disabled={state[field].length === 1}
              onClick={() => this.remove(rIndex)}
            />

            <Icon
              className="dynamic-add-button"
              type="plus-circle-o"
              onClick={() => this.add()}
            />
          </Col>
        </Row>);
      });
      return (<div>
        {rows}
        {/* <Row key='add' style={{width:'50%'}}>
                    <Col>
                        <Button type="dashed" onClick={this.add} style={{ width: '75%' }}>
                             <Icon type="plus" /> Add field
                        </Button>
                    </Col>
                </Row>*/}
      </div>);
    }
    return <div>Not found match component for {options.type}</div>;
  }
}

DynamicComponent.propTypes = {
  location: PropTypes.object,
  options: PropTypes.object,
  data: PropTypes.object,
  onChange: PropTypes.func,
};

export default DynamicComponent;

