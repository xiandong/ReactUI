import React, { PropTypes } from 'react';
import { Form, Button, Row, Col, Input, InputNumber, Radio, message } from 'antd';
import { Select } from 'antd';
const Option = Select.Option;
const FormItem = Form.Item;
const ButtonGroup = Button.Group;

class UserProfile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      mode: 'VIEW',
    };
  }
  edit =() => {
    this.setState({
      ...this.state,
      mode: 'EDIT',
    });
  }

	 onSave = (e) => {
	    e.preventDefault();
	    this.props.form.validateFieldsAndScroll((err, values) => {
	      if (!err) {
	        console.log('Received values of form: ', values);
	        this.props.onSave(values, () => {
	        	this.setState({ ...this.state, mode: 'VIEW' });
			    message.info('Save user profile successfully.');
	        });
	      }
	    });
	  }
	 onCancel =() => {
   this.setState({
     ...this.state,
     mode: 'VIEW',
   });
 }
  render() {
    const { user, preferences, metadata,
			// form: {
			//     getFieldDecorator,
			//     validateFields,
			//     getFieldsValue
			//   },
			 onSave,
			} = this.props;
    const formItemLayout = {
      labelCol: { span: 2 },
      		wrapperCol: { span: 14 },
    };
    const users = metadata[100];
		// users may be available when first load.
    const userOptions = users ? users.map((user) => {
	    	return (<Option key={user.id}>{user.label}</Option>);
    }) : [];
    return (
			// user is loading
			user && user.userId && preferences.timezone && preferences.timezone.id ?
  <div>
    <div style={{ marginBottom: 16, textAlign: 'right' }}>
      {
			      this.state.mode === 'VIEW' ?
  <Button size="large" type="primary" onClick={this.edit}>Edit</Button>
			      :
  <Button.Group type="primary">
    <Button size="large" type="primay" onClick={this.onSave}>Save</Button>
    <Button size="large" type="ghost" onClick={this.onCancel}>Cancel</Button>
  </Button.Group>
			   }
    </div>
    {/* JSON.stringify(user)*/}
    {/* JSON.stringify(preferences)*/}
    <Form horizontal>
      <FormItem label="Name" {...formItemLayout}>
        {

			  				`${user.username}`
			  			}
      </FormItem>
      <FormItem label="Email" {...formItemLayout}>
        {
			  				this.state.mode === 'EDIT' ?
			  				this.props.form.getFieldDecorator('user.email', {
			  					initialValue: user.email,
			  					rules: [
			  						{
			  							required: true,
			  							message: 'The user email is required.',
			  						},
			  						{
						              type: 'email', message: 'The input is not valid E-mail!',
						            },
			  					],
			  				})(<Input />)
			  				:
			  				`${user.email}`
			  			}
      </FormItem>
      <FormItem label="First Name" {...formItemLayout}>
        {
			  				this.state.mode === 'EDIT' ?
			  				this.props.form.getFieldDecorator('user.firstName', {
			  					initialValue: user.firstName,
			  					rules: [
			  						{
			  							required: true,
			  							message: 'The First Name is required.',
			  						},
			  					],
			  				})(<Input />)
			  				:
			  				`${user.firstName}`
			  			}
      </FormItem>
      <FormItem label="Last Name" {...formItemLayout}>
        {
			  				this.state.mode === 'EDIT' ?
			  				this.props.form.getFieldDecorator('user.lastName', {
			  					initialValue: user.lastName,
			  					rules: [
			  						{
			  							required: true,
			  							message: 'The user last name is required.',
			  						},
			  					],
			  				})(<Input />)
			  				:
			  				`${user.lastName}`
			  			}
      </FormItem>
      <FormItem label="Phone" {...formItemLayout}>
        {
			  				this.state.mode === 'EDIT' ?
			  				this.props.form.getFieldDecorator('user.phone', {
			  					initialValue: user.phone,
			  					rules: [
			  						{
			  							required: false,
			  						},
			  					],
			  				})(<Input />)
			  				:
			  				`${user.phone}`
			  			}
      </FormItem>
      <FormItem label="Adress" {...formItemLayout}>
        {
			  				this.state.mode === 'EDIT' ?
			  				this.props.form.getFieldDecorator('user.address', {
			  					initialValue: user.address,
			  					rules: [
			  						{
			  							required: false,
			  						},
			  					],
			  				})(<Input />)
			  				:
			  				`${user.address}`
			  			}
      </FormItem>
      <FormItem label="Manager" {...formItemLayout}>
        {
			  				this.state.mode === 'EDIT' ?
			  				this.props.form.getFieldDecorator('user.manager', {
			  					initialValue: user.manager.id,
			  					rules: [
			  						{
			  							required: true,
			  							message: 'The user manager is required.',
			  						},
			  					],
			  				})(
  <Select
    showSearch
    style={{ width: 200 }}
    placeholder="Select a person"
    optionFilterProp="children"
    filterOption={(input, option) => option.props.value.toLowerCase().indexOf(input.toLowerCase()) >= 0}
  >
    <Option value="lucy">lucy</Option>
    {userOptions}

  </Select>,
			  				)
			  				:
			  				`${user.manager.label}`
			  			}
      </FormItem>
      <FormItem label="Timezone" {...formItemLayout}>
        {preferences.timezone.label}
      </FormItem>
      <FormItem label="Language" {...formItemLayout}>
        {preferences.language.label}
      </FormItem>

    </Form>
  </div>
				: <div />
		  );
  }

}

UserProfile.propTypes = {
  onSave: PropTypes.func,
  user: PropTypes.object,
  preferences: PropTypes.object,
  form: PropTypes.object,
};

export default Form.create()(UserProfile);
