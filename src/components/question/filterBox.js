import React, { PropTypes } from 'react';
import { Menu, Icon, Card } from 'antd';
import { Link } from 'dva/router';

import styles from './filterBox.less';

const FilterBox = ({ name, label, value, resolvedValue, onClick }) => {
  return (
    <div className={styles.pillBox}>
      <a onClick={e => onClick(name)}>
        <h5>{label}</h5>
        <span className={styles.pillText}>{resolvedValue}</span>
        <span className={styles.pillCancel}><Icon type="close" /></span>
      </a>
    </div>
  );
};

FilterBox.propTypes = {
  props: PropTypes.object,
};

export default FilterBox;
