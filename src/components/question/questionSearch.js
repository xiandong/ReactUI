import React, { PropTypes } from 'react';
import { Menu, Icon, Row, Col, Button, Input } from 'antd';
import { Link } from 'dva/router';

import FilterBox from './filterBox';

import styles from './questionSearch.less';

const Search = Input.Search;

const QuestionSearch = ({ metadata,criteria, keyword, onCreate, onRemoveFilter, onFilter }) => {
    //       //the metadata may be not ready since this method is called by subscriptions.
          // resolve label field
          let criteriaRows = [];
          Object.keys(criteria).forEach((k) => {
              // FIXME resolve label and resolvedValue
              const label = k;
              let resolvedValue = criteria[k];
              if(k=='chapter' && metadata['chapters'] && metadata.chapters[criteria['chapter']]){
                resolvedValue=metadata.chapters[criteria['chapter']].value;
              }
              if(k=='module'){
                if(criteria['chapter'] &&  metadata['chapters'] && metadata.chapters[criteria['chapter']].modules
                && metadata.chapters[criteria['chapter']].modules[criteria['module']])
                  resolvedValue=metadata.chapters[criteria['chapter']].modules[criteria['module']].value;
              }
              criteriaRows.push({name: k, label, value: criteria[k], resolvedValue});
            });


  return (
    <Row gutter={24}>
      <Col lg={24}>
        <ul className={styles.filterBoxes}>
          {
                     criteriaRows.map((box, key) => {
                       const filterBoxProps = { ...box, onClick: onRemoveFilter };
                       return (<li key={key} className={styles.navPill}>
                         <FilterBox {...filterBoxProps} />
                       </li>);
                     },
                     )
                  }
        </ul>
      </Col>
      <Col lg={8} md={12} sm={16} xs={24} style={{ marginBottom: 16 }}>
        <Search
          placeholder="input search text"
          style={{ width: 500 }}
          onSearch={(value) => {
            console.log(value);
            onFilter(value);
          }
          }
        />
      </Col>
      <Col lg={{ offset: 8, span: 8 }} md={12} sm={8} xs={24} style={{ marginBottom: 16, textAlign: 'right' }}>
        <Button size="large" type="ghost" onClick={onCreate}>Add</Button>
      </Col>
    </Row>
  );
};

QuestionSearch.propTypes = {
  criteira: PropTypes.array,
  keyword: PropTypes.string,
  onCreate: PropTypes.func,
  onRemoveFilter: PropTypes.func,
};

export default QuestionSearch;
