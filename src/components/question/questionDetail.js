import React, { PropTypes } from 'react';
import { Form, Card, Button, Row, Col, Input, InputNumber, Radio, message,Dropdown,Menu  } from 'antd';
import { Select, Icon } from 'antd';
import { Link } from 'dva/router';
import Markdown from '../../utils/markdown';
import DynamicComponent from '../dynamicComponent';
import QuestionInput from './questionInput';
import QuestionHeader from './questionHeader';
import QuestionFooter from './questionFooter';
const Option = Select.Option;
const FormItem = Form.Item;
const ButtonGroup = Button.Group;
const { TextArea } = Input;

class QuestionDetail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      mode: 'REGULAR',
    };
  }
   handleButtonClick=(e) => {
    message.info('Click on left button.');
    console.log('click left button', e);
  }
   handleMenuClick=(e) => {
    message.info('Click on menu item.');
    console.log('click', e);
    if(e.key=="1")
      this.setState({...this.state,mode: 'EDIT'});
  }
  updateQuestion=(e) => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
        //values.detail.content=values.detail.content.formatJSON();
        this.props.updateQuestion(values, () => {
          this.setState({ ...this.state, mode: 'REGULAR' });
        message.info('Save user profile successfully.');
        });
      }
    });
  }
  cancelEdit=() => {
    this.setState({...this.state,mode: 'REGULAR'});
  }
  // handleSubmit = (e) => {
  //   e.preventDefault();
  //   this.props.form.validateFields((err, values) => {
  //     if (!err) {
  //       console.log('Received values of form: ', values);
  //       console.log(`handleSubmit:${JSON.stringify(this.props.form.getFieldsValue())}`);
  //       if(!values.question){
  //         message.error('You didn\'t answer the question');
  //         return;
  //       }

  //       this.props.answerQuestion({answer:values.question});
  //     }
  //   });
  // }
  // checkAnswer = (rule, value, callback) => {
  //   console.log(`checkAnswer value=${value}`);
  //   console.log(`check answer:${JSON.stringify(this.props.form.getFieldsValue())}`);
  //   console.log(`check answer:${JSON.stringify(this.props.form.getFieldValue('question'))}`);
  //   callback();
  // }
  render() {
    const menu = (
      <Menu onClick={this.handleMenuClick}>
      <Menu.Item key="1">Edit</Menu.Item>
      <Menu.Item key="2">UnLock</Menu.Item>
      <Menu.Item key="3">Delete</Menu.Item>
    </Menu>
    )
    const { getFieldDecorator } = this.props.form;
    const questionProps = this.props;
    const {detail}=questionProps;
    let {locked}=questionProps;
    if(!locked)
      locked=false;
      console.log('question is :'+JSON.stringify(questionProps));
    const { answer } = this.props.detail;
    const formItemLayout = {
      labelCol: { span: 14 },
      wrapperCol: { span: 10 },
    };
    const editFormItemLayout = {
      labelCol: { span: 4 },
      wrapperCol: { span: 10 },
    };
    const headerProps={...this.props.navigator,
        goBack:questionProps.onBack,
        goPrevious:questionProps.onPrevious,
        goNext:questionProps.onNext
      };
    const footerProps={
      comments:questionProps.comments,
      onAdd:questionProps.onAddComment
    };
    return (
      <div>
        <QuestionHeader  {...headerProps}/>
        <Row style={{ paddingTop: '8px' }}>
          <Col span={20}>
          <Row>
            <Col span={12}></Col>
            <Col span={12} style={{ textAlign: 'right' }}>
            <Dropdown overlay={menu}>
              <Button style={{ marginLeft: 8 }}>
                Actions <Icon type="down" />
              </Button>
            </Dropdown>
            </Col>
          </Row>
            <Row>
              <Col span={24}>
                <div style={{ padding: '30px' }}>
                  { this.state.mode==='REGULAR' &&
                      <Row style={{minHeight:'200px'}}>
                        <Col span={18}>
                          <QuestionInput {...questionProps} />
                        </Col>
                        <Col>
                        {
                          locked?
                          <Icon type="lock" style={{ fontSize: 30, color: '#08c',float:'right' }}/>
                          : ''
                        }
                        </Col>
                      </Row>
                  }
                  { this.state.mode==='EDIT' &&
                       <Form>
                        <FormItem label="Name" {...editFormItemLayout}>
                          {
                            	this.props.form.getFieldDecorator('detail.name', {
                                initialValue: detail.name,
                                rules: [
                                  {
                                    required: true,
                                    message: 'The name is required.',
                                  }],
                              })(<Input />)
                          }
                        </FormItem>
                        <FormItem label="Content" {...editFormItemLayout}>
                          {
                            	this.props.form.getFieldDecorator('detail.content', {
                                initialValue: detail.content,
                                rules: [
                                  {
                                    required: true,
                                    message: 'The content is required.',
                                  }],
                              })(<TextArea rows={8}/>)
                          }
                        </FormItem>
                        <div style={{ marginBottom: 16, textAlign: 'right' }}>
                          <Button type="primary" onClick={this.updateQuestion} >Save</Button>
                          <Button type="default" onClick={this.cancelEdit}>Cancel</Button>
                        </div>
                       </Form>
                  }
                </div>
              </Col>
            </Row>
          </Col>
          <Col span={4}>
            <Card>
              <Row>
                <Col>
                  <FormItem label="Assigner" {...formItemLayout}>
                  {questionProps.detail.creator}
			  		</FormItem>
                </Col>
                <Col>
                  <FormItem label="Last Modified Date" {...formItemLayout}>
                  {questionProps.detail.createDate}
			  		</FormItem>
                </Col>
              </Row>
            </Card>
          </Col>
        </Row>

        Answer:<pre>{JSON.stringify(this.props.detail.answer)}</pre>
        <QuestionFooter {...footerProps}/>
      </div>
    );
  }
}

QuestionDetail.propTypes = {
  anwerQuestion: PropTypes.func,
  updateQuestion: PropTypes.func,
  detail: PropTypes.object,
  comments: PropTypes.array,
  files: PropTypes.array,
  history: PropTypes.array,
  form: PropTypes.object,
  navigator:PropTypes.object,
  onBack: PropTypes.func,
  onPrevious: PropTypes.func,
  onNext: PropTypes.func,
  onAddComment: PropTypes.func,
};

export default Form.create()(QuestionDetail);
