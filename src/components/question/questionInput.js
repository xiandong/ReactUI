import React, { PropTypes } from 'react';
import { Form, Button, Row, Col, Input, InputNumber, Radio, message } from 'antd';
import { Select } from 'antd';
import * as Survey from "survey-react";
import "bootstrap/dist/css/bootstrap.css";
import Markdown from '../../utils/markdown';
import DynamicComponent from '../dynamicComponent';
const Option = Select.Option;
const FormItem = Form.Item;
const ButtonGroup = Button.Group;

import styles from './questionInput.less';

//sample for surveyjs
////https://github.com/surveyjs/surveyjs_react_quickstart
class QuestionInput extends React.Component {
  constructor(props) {
    super(props);
    //this.onCompleteSurvey = this.onCompleteSurvey.bind(this);
  }
  onCompleteSurvey=(survey)=> {
    console.log('survey.data='+JSON.stringify(survey.data));
    this.props.answerQuestion(survey.data);
  }
  render() {
    const contentJson = this.props.detail.content;
    const answer = this.props.detail.answer;
    if(contentJson && !contentJson.isJSON())
      return <span>Invalid content</span>
    window.survey = new Survey.Model(contentJson?JSON.parse(contentJson):'');
    //Survey.Survey.cssType = "bootstrap";
    Survey.StylesManager.applyTheme("default");
    survey.data = answer;
    survey.mode = this.props.locked?'display':'edit';
    survey.completeText='Answer';
    return (
      contentJson ?
          // <Markdown options={{ overrides: {} }} data={answer} onChange={this.onChange} readOnly={this.props.locked}>
          //   {content}
          // </Markdown>
          <Survey.Survey model={survey} onComplete={this.onCompleteSurvey}/>
        : <div />
    );
  }
}
QuestionInput.propTypes = {
  onChange: PropTypes.func,
  value: PropTypes.object,
};
export default QuestionInput;
