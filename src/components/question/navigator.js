import React, { PropTypes } from 'react';
import { Card, Icon, Row, Col, Button, Input,Form } from 'antd';
import { Link } from 'dva/router';
const FormItem = Form.Item;
import prevIcon from '../../assets/icon-previous.png';
import nextIcon from '../../assets/icon-next.png';


const Navigator = () => {
    const formItemLayout={
			labelCol: { span:4 },
      		wrapperCol: { span: 4 }
		}
    return (
        <div style={{marginBottom: 16, textAlign: 'right'}}>
               <Button style={{marginRight:'30px'}} >
                        <Icon type="rollback" />Back
               </Button>
               <Button style={{marginRight:'30px'}} >
                        <Icon type="left" />Previous
               </Button>
               <Button >
                        <Icon type="right" />Next
               </Button>
		</div>
    );
};

Navigator.propTypes = {
};

export default Navigator;
