import React, { PropTypes } from 'react';
import {upload} from '../../utils/request';
import { Menu, Icon, Row, Col, Button, Input, Tabs } from 'antd';
import { List, Avatar } from 'antd';
import { Link } from 'dva/router';
import { EditorState, convertToRaw, ContentState } from 'draft-js';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import { Editor } from 'react-draft-wysiwyg';
import * as Survey from "survey-react";
//import "bootstrap/dist/css/bootstrap.css";
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import styles from './questionComments.less';

const TabPane = Tabs.TabPane;
//https://jpuri.github.io/react-draft-wysiwyg/#/ mention?
//https://github.com/surveyjs/surveyjs_react_quickstart
class QuestionComments extends React.Component {
  constructor(props) {
    super(props);
    this.state = {editorState: EditorState.createEmpty()};
    this.onChange = (editorState) => this.setState({editorState});
  }
  showEditor =() => {
    this.setState({
      ...this.state,
      editorState:EditorState.createEmpty(),
      showEditor: true
    });
  }
  add =() => {
    console.log(this.state.editorState.text);
    const raw=draftToHtml(convertToRaw(this.state.editorState.getCurrentContent()));
    console.log(raw);
    this.props.onAdd({value:raw}, () => {
      this.setState({
        ...this.state,
        editorState:EditorState.createEmpty(),
        showEditor: false
      });
    });

  }
  cancel =() => {
    this.setState({
      ...this.state,
      editorState:EditorState.createEmpty(),
      showEditor: false
    });
  }

  edit =(id) => {

  }

  delete =(id) => {

  }

  uploadImageCallBack=(file) =>{
    return upload('/v1/upload/',file);
  }

  render() {
    const { editorState } = this.state;
    // const comments=this.props.comments.map((comment)=>{
    //   return (<div>{comment.creator} added a comment - {comment.createdDate}</div>);
    // });
    const listData=this.props.comments;
    const pagination = {
      pageSize: 10,
      current: 1,
      total: listData?listData.length:0,
      onChange: (() => {}),
    };
    const comments=
        <List
            itemLayout="vertical"
            size="large"
            pagination={pagination}
            dataSource={listData}
            renderItem={item => (
              <List.Item actions={[<a onClick={this.edit(item.id)}>edit</a>, <a onClick={this.delete(item.id)}>delete</a>]}>
                <List.Item.Meta
                  title={<span>{item.system_fields.created_by} added a comment - {item.system_fields.created_at}</span>}
                />
                <span dangerouslySetInnerHTML={{__html:item.content}} />
              </List.Item>
            )}
          />

    return (
      <div>
        {comments}
        <Button onClick={this.showEditor}> <Icon type="message" />Comment</Button>
        {this.state.showEditor &&
        <div style={{width:"60%"}}>
        <Editor
          wrapperClassName="comment-wrapper"
          editorClassName="comment-editor"
          mention={{
            separator: ' ',
            trigger: '@',
            suggestions: [
              { text: 'APPLE', value: 'apple', url: 'apple' },
              { text: 'BANANA', value: 'banana', url: 'banana' },
              { text: 'CHERRY', value: 'cherry', url: 'cherry' },
              { text: 'DURIAN', value: 'durian', url: 'durian' },
              { text: 'EGGFRUIT', value: 'eggfruit', url: 'eggfruit' },
              { text: 'FIG', value: 'fig', url: 'fig' },
              { text: 'GRAPEFRUIT', value: 'grapefruit', url: 'grapefruit' },
              { text: 'HONEYDEW', value: 'honeydew', url: 'honeydew' },
            ],
          }}
          editorState={editorState}
          onEditorStateChange={this.onChange}
          toolbar={{
            image: { uploadCallback: this.uploadImageCallBack, alt: { present: true, mandatory: true } }
          }}
          />
          <div style={{ marginBottom: 16, textAlign: 'right' }}>
            <Button.Group type="primary">
              <Button size="large" type="primay" onClick={this.add}>Add</Button>
              <Button size="large" type="ghost" onClick={this.cancel}>Cancel</Button>
            </Button.Group>
          </div>
        </div>
        }
      </div>
    );
  };
}

QuestionComments.propTypes = {
  comments: PropTypes.array,
  onAdd: PropTypes.func
};

export default QuestionComments;
