import React, { PropTypes } from 'react';
import { Menu, Icon, Row, Col, Button, Input, Tabs } from 'antd';
import { Link } from 'dva/router';

import QuestionComments from './questionComments';
import QuestionFiles from './questionFiles';
import QuestionHistory from './questionHistory';

const TabPane = Tabs.TabPane;

const QuestionFooter = (comments) => {
  return (
    <Tabs type="card">
      <TabPane tab="Comments" key="1">
        <QuestionComments {...comments}/>
      </TabPane>
      <TabPane tab="Files" key="2">
        <QuestionFiles />
      </TabPane>
      <TabPane tab="History" key="3">
        <QuestionHistory />
      </TabPane>
    </Tabs>
  );
};

QuestionFooter.propTypes = {
  comments: PropTypes.array,
};

export default QuestionFooter;
