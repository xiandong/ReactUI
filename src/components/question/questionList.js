import React, {PropTypes} from 'react';
import {Menu, Icon, Table, Pagination} from 'antd';
import {Link} from 'dva/router';
import moment from 'moment';

import Markdown from '../../utils/markdown';

import styles from './questionList.less';

class QuestionList extends React.Component {
  constructor(props) {
    super(props);
  }
  state = {
    selectedRowKeys: [], // Check here to configure the default column
    filteredInfo: null,
    sortedInfo: null,
    loading: false
  };

  onSelectChange = (selectedRowKeys) => {
    console.log('selectedRowKeys changed: ', selectedRowKeys);
    this.setState({selectedRowKeys});
  }

  handleChange = (pagination, filters, sorter) => {
    console.log('Various parameters', pagination, filters, sorter);
    this.setState({filteredInfo: filters, sortedInfo: sorter});
  }
  clearFilters = () => {
    this.setState({filteredInfo: null});
  }
  clearAll = () => {
    this.setState({filteredInfo: null, sortedInfo: null});
  }

  openQuestion=(index, id) => {
    console.log('index='+index+' id='+id+' onOpenQuestion'+this.props.onOpenQuestion+'  openQuestion='+this.props.openQuestion);
    this.props.openQuestion({id,index});

  }

  render() {
    const {loading, selectedRowKeys} = this.state;
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange
    };
    const pagination = {
      defaultPageSize: 50,
      showSizeChanger: true,
      showTotal: (total, range) => `Total ${total} records`
    };

    let {sortedInfo, filteredInfo} = this.state;
    sortedInfo = sortedInfo || {};
    filteredInfo = filteredInfo || {};

    const {data} = this.props;
    const columns = [
      {
        title: '#',
        dataIndex: 'id',
        key: 'id',
        //render: text => <a href="#">{text}</a>,
        className: styles.tableHeader,
        //sorter: (a, b) => a.id - b.id,
        //sortOrder: sortedInfo.columnKey === 'id' && sortedInfo.order,
        // filteredValue: filteredInfo.name || null, onFilter: (value, record) =>
        // record.name.includes(value)
        render: (text, record, index) => (
          //<Link to={`/questions/${record.id}`}>{index + 1}</Link>
          <Link onClick={(e) => this.openQuestion(index,record.id)}>{index + 1}</Link>
        )
      }, {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
        width: '400px',
        render: (text, record) => (
          // <Markdown
          //   options={{
          //   overrides: {
          //     xcode: {
          //       component: 'skipXcode'
          //     }
          //   }
          // }}
          //   data={record.answer}>
          //   {record.content}
          // </Markdown>
          text
        ),
        // filters: [
        //   {
        //     text: 'London',
        //     value: 'London'
        //   }, {
        //     text: 'New York',
        //     value: 'New York'
        //   }
        // ],
        // filteredValue: filteredInfo.address || null,
        // onFilter: (value, record) => record
        //   .address
        //   .includes(value),
        sorter: (a, b) =>  a.name.localeCompare(b.name),
        sortOrder: sortedInfo.columnKey === 'name' && sortedInfo.order
      }, {
        title: 'Status',
        dataIndex: 'locked',
        key: 'locked',
        sorter: (a, b) => a.locked>b.locked,
        sortOrder: sortedInfo.columnKey === 'locked' && sortedInfo.order,       
        render: (text, record) => (
          text?"Yes":"No"
        )
      }, {
        title: 'Action',
        key: 'action'
      }
    ];

    return (<Table
      rowSelection={rowSelection}
      columns={columns}
      dataSource={data}
      pagination={pagination}
      onChange={this.handleChange}/>);
  }
}

QuestionList.propTypes = {
  data: PropTypes.array,
  openQuestion: PropTypes.func,
};

export default QuestionList;
