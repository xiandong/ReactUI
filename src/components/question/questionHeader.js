import React, { PropTypes } from 'react';
import { Card, Icon, Row, Col, Button, Input, Form,Dropdown,Menu } from 'antd';
import { Link } from 'dva/router';
const FormItem = Form.Item;


const QuestionHeader = ({back:canBack, previous:canPrevious,next:canNext,goBack,goPrevious,goNext}) => {
  const formItemLayout = {
    labelCol: { span: 4 },
      		wrapperCol: { span: 4 },
  };

  return (
    <div>
      <Row>
        <Col span={12}>
          { <Button type="primary" disabled={!canBack} onClick={goBack}>
            <Icon type="rollback" />Back
               </Button> }
        </Col>
        <Col span={12} style={{ textAlign: 'right' }}>
          <Button type="default" disabled={!canPrevious} onClick={goPrevious}>
            <Icon type="left" />Previous
          </Button>
          <Button type="default" disabled={!canNext} onClick={goNext}>
            <Icon type="right" />Next
                 </Button>
        </Col>
      </Row>
    </div>
  );
};

QuestionHeader.propTypes = {
   canBack:PropTypes.bool,
   canPrevious:PropTypes.bool,
   canNext:PropTypes.bool,
   goBack: PropTypes.func,
   goPrevious: PropTypes.func,
   goNext: PropTypes.func,
};

export default QuestionHeader;
