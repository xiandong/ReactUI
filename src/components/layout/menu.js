import React from 'react';
import { Menu, Icon } from 'antd';
import { Link } from 'dva/router';
// import { menu } from '../../utils'
const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;

const getMenus = function (menuArray, topMenus, siderFold, parentPath) {
  parentPath = parentPath || '/';
  return menuArray.map((item) => {
    if (item.child) {
      return (
        <Menu.SubMenu key={item.key} title={<span>{item.icon ? <Icon type={item.icon} /> : ''}{siderFold && topMenus.indexOf(item.key) >= 0 ? '' : item.name}</span>}>
          {getMenus(item.child, topMenus, siderFold, `${parentPath + item.key}/`)}
        </Menu.SubMenu>
      );
    }
    return (
      <Menu.Item key={item.key}>
        <Link to={`${item.link}`}>
          {item.icon ? <Icon type={item.icon} /> : ''}
          {siderFold && topMenus.indexOf(item.key) >= 0 ? '' : item.name}
        </Link>
      </Menu.Item>
    );
  });
};

function Menus({ menu, siderFold, darkTheme, location, handleClickNavMenu, navOpenKeys, changeOpenKeys,
    loadChapters, loadModules, loadSegments }) {
  const topMenus = menu.map(item => item.key);
  const menuItems = getMenus(menu, topMenus, siderFold);

  const onOpenChange = (openKeys) => {
    const latestOpenKey = openKeys.find(key => !(navOpenKeys.indexOf(key) > -1));
    const latestCloseKey = navOpenKeys.find(key => !(openKeys.indexOf(key) > -1));
    let nextOpenKeys = [];
    if (latestOpenKey) {
      nextOpenKeys = getAncestorKeys(latestOpenKey).concat(latestOpenKey);
    }
    if (latestCloseKey) {
      nextOpenKeys = getAncestorKeys(latestCloseKey);
    }
    changeOpenKeys(nextOpenKeys);
  };
  const getAncestorKeys = (key) => {
    const map = {
      navigation2: ['navigation'],
    };
    return map[key] || [];
  };
  // 菜单栏收起时，不能操作openKeys
  const menuProps = !siderFold ? {
    onOpenChange,
    openKeys: navOpenKeys,
  } : {};

  /* return (
    <Menu
      {...menuProps}
      mode={siderFold ? 'vertical' : 'inline'}
      theme={darkTheme ? 'dark' : 'light'}
      onClick={handleClickNavMenu}
      defaultSelectedKeys={[location.pathname.split('/')[location.pathname.split('/').length - 1] || 'dashboard']}>
      {menuItems}
    </Menu>
  )*/
  function handleClick(e) {
    console.log('click', e);
  }
  return (
    <Menu
      mode={siderFold ? 'vertical' : 'inline'}
      theme={darkTheme ? 'dark' : 'light'}
      defaultSelectedKeys={[location.pathname.split('/')[location.pathname.split('/').length - 1] || 'dashboard']}
    >
      {menuItems}
    </Menu>
  );
 /* return (
  <Menu onClick={handleClick}  mode="inline">
<SubMenu key="sub1" title={<span><Icon type="mail" /><span>导航一</span></span>}>
<MenuItemGroup title="分组1">
<Menu.Item key="1">选项1</Menu.Item>
<Menu.Item key="2">选项2</Menu.Item>
</MenuItemGroup>
<MenuItemGroup title="分组2">
<Menu.Item key="3">选项3</Menu.Item>
<Menu.Item key="4">选项4</Menu.Item>
</MenuItemGroup>
</SubMenu>
<SubMenu key="sub2" title={<span><Icon type="appstore" /><span>Manufactory Legal Information</span></span>}>
<Menu.Item key="5">选项5</Menu.Item>
  <SubMenu key="sub3.1" title="111三级导航">
<Menu.Item key="6">选项6</Menu.Item>
  </SubMenu>
<SubMenu key="sub3" title="三级导航">
<Menu.Item key="7">选项7</Menu.Item>
<Menu.Item key="8">选项8</Menu.Item>
</SubMenu>
</SubMenu>
<SubMenu key="sub4" title={<span><icon type="setting" /><span>导航三</span></span>}>
<Menu.Item key="9">选项9</Menu.Item>
<Menu.Item key="10">选项10</Menu.Item>
<Menu.Item key="11">选项11</Menu.Item>
<Menu.Item key="12">选项12</Menu.Item>
<Menu.Item key="13">选项13</Menu.Item>
<Menu.Item key="14">选项14</Menu.Item>
<Menu.Item key="15">选项15</Menu.Item>
<Menu.Item key="16">选项16</Menu.Item>
<Menu.Item key="17">选项17</Menu.Item>
<Menu.Item key="18">选项18</Menu.Item>
<Menu.Item key="19">选项19</Menu.Item>
</SubMenu>
</Menu>
 )*/
}

export default Menus;
