import React from 'react';
import { Icon, Switch } from 'antd';
import styles from './main.less';
import { config } from '../../utils';
import Menus from './menu';

function Sider({ menu, siderFold, darkTheme, location, changeTheme,
  navOpenKeys, changeOpenKeys, loadChapters, loadModules, loadSegments }) {
  const menusProps = {
    menu,
    siderFold,
    darkTheme,
    location,
    navOpenKeys,
    changeOpenKeys,
    loadChapters,
    loadModules,
    loadSegments,
  };
  return (
    <div>
      <div className={styles.logo}>
        <img src={config.logoSrc} />
        {siderFold ? '' : <span>{config.logoText}</span>}
      </div>
      <Menus {...menusProps} />
      {!siderFold ? <div className={styles.switchtheme}>
        <span><Icon type="bulb" />Change Theme</span>
        <Switch onChange={changeTheme} defaultChecked={darkTheme} checkedChildren="Black" unCheckedChildren="White" />
      </div> : ''}
    </div>
  );
}

export default Sider;
