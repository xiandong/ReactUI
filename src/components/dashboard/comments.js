import React from 'react';
import { Table, Tag, Button } from 'antd';
import styles from './comments.less';
import { color } from '../../utils';

const status = {
  1: {
    color: color.green,
    text: 'APPROVED',
  },
  2: {
    color: color.yellow,
    text: 'PENDING',
  },
  3: {
    color: color.red,
    text: 'REJECTED',
  },
};

function Comments(props) {
  const columns = [
    {
      title: 'avatar',
      dataIndex: 'avatar',
      width: 48,
      className: styles.avatarcolumn,
      render: text => <span style={{ backgroundImage: `url(${text})` }} className={styles.avatar} />,
    }, {
      title: 'content',
      dataIndex: 'content',
      render: (text, it) => <div>
        <h5 className={styles.name}>{it.name}</h5>
        <p className={styles.content}>{it.content}</p>
        <div className={styles.daterow}>
          {/* <Tag color={status[it.status].color}>{status[it.status].text}</Tag>*/}
          <Button size="small" type="primary" >Open</Button>
          <span className={styles.date}>{it.date}</span>
        </div>
      </div>,
    },
  ];
  return (
    <div className={styles.comments}>
      <Table pagination={{ pageSize: 5 }} scroll={{ y: 350 }} showHeader={false} columns={columns} rowKey={(record, key) => key} dataSource={props.data.filter((item, key) => key > 0)} />
    </div>
  );
}

export default Comments;
