import React, { PropTypes } from 'react';
import { Menu, Icon } from 'antd';
import { Link } from 'dva/router';

const Example = ({ location, children }) => {
  return (
    <div>
      <Menu
        selectedKeys={[location.pathname]}
        mode="horizontal"
        theme="dark"
      >
        <Menu.Item key="/">
          <Link to="/"><Icon type="bars" />Home</Link>
        </Menu.Item>
        <Menu.Item key="/example">
          <Link to="/example"><Icon type="home" />Example</Link>
        </Menu.Item>
        <Menu.Item key="/404">
          <Link to="/page-you-dont-know"><Icon type="frown-circle" />404</Link>
        </Menu.Item>
        <Menu.Item key="/antd">
          <a href="https://github.com/dvajs/dva">dva</a>
        </Menu.Item>
      </Menu>
      <div> {children}</div>
    </div>
  );
};

Example.propTypes = {
  location: PropTypes.object,
  // dispatch: PropTypes.func,
  // loading: PropTypes.object,
  // loginButtonLoading: PropTypes.bool,
  // login: PropTypes.bool,
  // user: PropTypes.object,
  // siderFold: PropTypes.bool,
  // darkTheme: PropTypes.bool
};

export default Example;
