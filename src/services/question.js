
import request from '../utils/request';
import {
  storageTokenKey,
} from '../utils/constants';

export function createQuestion({
  organization,
  segment,
  name,
  content,
  assignee,
  dueDate,
}) {
  const token = window.localStorage.getItem(storageTokenKey);
  return request(`/v1/segments/${segment}/questions`, {
    method: 'POST',
    headers: new Headers({
      'Content-Type': 'application/json; charset=utf-8',
      Authorization: `Bearer ${token}`,
      Origin:'http://localhost'
    }),
    body: JSON.stringify({
      "name": `${name}`,
      "organization": organization,
      "content": {
        "text": `${content}`
      }
    }),
  });
}

export function updateQuestion({
  id,
  name,
  description,
  content
}) {
  const token = window.localStorage.getItem(storageTokenKey);
  return request(`/v1/questions/${id}`, {
    method: 'PATCH',
    headers: new Headers({
      'Content-Type': 'application/json; charset=utf-8',
      Authorization: `Bearer ${token}`,
    }),
    body: JSON.stringify({
      update_mask:"name,description,content",
      name:name,
      description:description,
      content:{text:content}
    }),
  });
}
// load list of questions per criteria
export function fetchQuestions(criteria) {
  const token = window.localStorage.getItem(storageTokenKey);
  return request('/v1/questions?organization=1', {
    method: 'GET',
    headers: new Headers({
      //'Content-Type': 'application/json; charset=utf-8',
      Authorization: `Bearer ${token}`,
    }),
    //body: JSON.stringify(criteria),
  });
}

export function fetchQuestion({
  id,
}) {
  const token = window.localStorage.getItem(storageTokenKey);
  // return request('/api/questions/'+id, {
  return request(`/v1/questions/${id}`, {
    method: 'GET',
    headers: new Headers({
      'Content-Type': 'application/json; charset=utf-8',
      Authorization: `Bearer ${token}`,
    }),
  });
}

export function fetchAnswer({
  id,
}) {
  const token = window.localStorage.getItem(storageTokenKey);
  // return request('/api/questions/'+id, {
  return request(`/v1/questions/${id}/answer`, {
    method: 'GET',
    headers: new Headers({
      'Content-Type': 'application/json; charset=utf-8',
      Authorization: `Bearer ${token}`,
    }),
  });
}

export function updateAnswer({
  id,answer,correctness
}) {
  const token = window.localStorage.getItem(storageTokenKey);
  const value={
    "content": answer,
    "correctness": correctness
  };
  // return request('/api/questions/'+id, {
  return request(`/v1/questions/${id}/answer`, {
    method: 'PUT',
    headers: new Headers({
      'Content-Type': 'application/json; charset=utf-8',
      Authorization: `Bearer ${token}`,
      Origin:'http://localhost'
    }),
    body: JSON.stringify(value),
  });
}

export function addComment({
  id,
  comment,
}) {
  const token = window.localStorage.getItem(storageTokenKey);
  return request(`/v1/comments`, {
    method: 'POST',
    headers: new Headers({
      'Content-Type': 'application/json; charset=utf-8',
      Authorization: `Bearer ${token}`,
    }),
    body: JSON.stringify({
      question_id:id,
      content:comment,
    }),
  });
}

export function fetchComments({
  id,
}) {
  const token = window.localStorage.getItem(storageTokenKey);
  return request(`/v1/questions/${id}/comments`, {
    method: 'GET',
    headers: new Headers({
      //'Content-Type': 'application/json; charset=utf-8',
      Authorization: `Bearer ${token}`,
    }),
  });
}

export function fetchHistory({
  id,
}) {
  const token = window.localStorage.getItem(storageTokenKey);
  return request(`/api/questions/${id}/history`, {
    method: 'GET',
    headers: new Headers({
      'Content-Type': 'application/json; charset=utf-8',
      Authorization: `Bearer ${token}`,
    }),
  });
}
