import { request } from '../utils';
import { storageTokenKey } from '../utils/constants';

export async function myCity(params) {
  return request('http://www.zuimeitianqi.com/zuimei/myCity', {
    method: 'get',
    cross: true,
    data: params,
  });
}

export async function queryWeather(params) {
  return request('http://www.zuimeitianqi.com/zuimei/queryWeather', {
    method: 'get',
    cross: true,
    data: params,
  });
}

export async function query(params) {
  const token = window.localStorage.getItem(storageTokenKey);
  return request('/api/dashboard', {
    method: 'get',
    data: params,
    headers: new Headers({
      Authorization: `Bearer ${token}`,
    }),
  });
}

