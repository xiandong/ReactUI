
import request from '../utils/request';
import { storageTokenKey } from '../utils/constants';

export function createUser({ username, password, email }) {
  return request('/api/user', {
    method: 'POST',
    headers: new Headers({
      'Content-Type': 'application/json; charset=utf-8',
      Authorization: `Bearer ${token}`,
    }),
    body: JSON.stringify({
      username, password, email,
    }),
  });
}

// basic information
export function fetchUser() {
  const token = window.localStorage.getItem(storageTokenKey);
  return request('/v1/profile', {
    method: 'get',
    headers: new Headers({
      Authorization: `Bearer ${token}`,
    }),
  });
}
// profile
export function fetchUserDetail() {
  const token = window.localStorage.getItem(storageTokenKey);
  return request('/api/userDetail', {
    method: 'get',
    headers: new Headers({
      Authorization: `Bearer ${token}`,
    }),
  });
}

export function fetchUserPreferences() {
  const token = window.localStorage.getItem(storageTokenKey);
  return request('/api/userPreferences', {
    method: 'get',
    headers: new Headers({
      Authorization: `Bearer ${token}`,
    }),
  });
}

export function saveUser({ user, preferences }) {
  return request('/api/user', {
    method: 'PUT',
    headers: new Headers({
      'Content-Type': 'application/json; charset=utf-8',
    }),
    body: JSON.stringify({
      user, preferences,
    }),
  });
}

export function changPassword({ userId, password }) {
  return request('/api/user', {
    method: 'PUT',
    headers: new Headers({
      'Content-Type': 'application/json; charset=utf-8',
    }),
    body: JSON.stringify({
      userId, password,
    }),
  });
}
