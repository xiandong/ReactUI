
import request from '../utils/request';
import { storageTokenKey } from '../utils/constants';

export function auth(payload) {
  return request('/v1/token', {
    method: 'post',
    headers: new Headers({
      'Content-Type': 'application/json; charset=utf-8',
    }),
    body: JSON.stringify({
      ...payload,
      grant_type: 'password',
      client_id: 'simpli-ui',
    }),
  });
}

export function loadMenu() {
  const token = window.localStorage.getItem(storageTokenKey);
  // return request('/api/menu', {
  //   method: 'get',
  //   headers: new Headers({
  //     Authorization: `Bearer ${token}`,
  //   }),
  // });
  //UI have menu list logic, will filter out per user permission.
  const menus=[{
			key: 'dashboard',
			name: 'Dashboard',
			icon: 'laptop',
			link:'dashboard',
		},
		{
			key: 'users',
			name: 'User Management',
			icon: 'user',
			link:'user',
		},
		{
			key: 'factories',
			name: 'Factory Management',
			icon: 'user',
			link:'factory',
    },
    {
      key: 'questionaire',
      name: 'Questionaire',
      icon: 'solution',
      child:[]
    }];
  return new Promise(function(resolve, reject) {
    resolve(menus);
  }).then(data => ({ data }))
      .catch((err) => { throw err; });;
}

export function loadMetadata() {
  const token = window.localStorage.getItem(storageTokenKey);
  return request('/api/metadata', {
    method: 'get',
    headers: new Headers({
      Authorization: `Bearer ${token}`,
    }),
  });
}

export function getChapters() {
  const token = window.localStorage.getItem(storageTokenKey);
  return request('/v1/chapters?organization=1', {
    method: 'get',
    headers: new Headers({
      Authorization: `Bearer ${token}`,
    }),
  });
}
export function getModules({ chapterId }) {
  const token = window.localStorage.getItem(storageTokenKey);
  return request(`/v1/chapters/${chapterId}/modules`, {
    method: 'get',
    headers: new Headers({
      Authorization: `Bearer ${token}`,
    }),
  });
}
export function getSegments({ chapterId,moduleId }) {
  const token = window.localStorage.getItem(storageTokenKey);
  return request(`/v1/modules/${moduleId}/segments`, {
    method: 'get',
    headers: new Headers({
      Authorization: `Bearer ${token}`,
    }),
  });
}
// export function getSegments({moduleId}) {
//     const token = window.localStorage.getItem(storageTokenKey);
//     return request('/v1/modules/'+moduleId+'/segments', {
//         method: 'get',
//         headers: new Headers({
//             "Authorization": `Bearer ${token}`
//         })
//     });
// }
