import React, { PropTypes } from 'react';
import { routerRedux, Link } from 'dva/router';
import { connect } from 'dva';
import { Row, Col, Card, Input, Table, Icon, Button } from 'antd';

import Markdown from '../utils/markdown';
import QuestionCard from '../components/question/questionCard';
import QuestionList from '../components/question/questionList';
import QuestionSearch from '../components/question/questionSearch';

const Search = Input.Search;

import styles from './questions.less';

function Questions({ location, dispatch, metadata,questions, children }) {
  console.log(`questions=${questions}`);
    // const {segment}=questions;
    // const {id}=segment;
  const { data } = questions;
  const questionListProps = { data,openQuestion:function(payload){
    console.log('openQuestion payload='+payload);
    dispatch({ type: 'questions/openQuestion',payload:payload });
  } };
    // filterBox
  const { criteria } = questions;
    // filter
  const { keyword } = questions.filter;

  const searchProps = {
    metadata,
    criteria,
    keyword,
    onCreate() {
      console.log('onCreate()');
      dispatch({ type: 'questions/create' });
    },
    onRemoveFilter(name) {
      const updatedCriteria = _.omit(criteria,name);
      dispatch({type: 'questions/fetch',payload: updatedCriteria});
      dispatch({ type: 'questions/updateCriteria', payload: updatedCriteria });
    },
    onFilter(fieldValue) {

    },
  };
  return (
    <div>
      <Row gutter={24}>
        <Col span={6}>
          <QuestionCard />
        </Col>
        <Col span={6}>
          <QuestionCard />
        </Col>
        <Col span={6}>
          <QuestionCard />
        </Col>
        <Col span={6}>
          <QuestionCard />
        </Col>
      </Row>
      <QuestionSearch {...searchProps} />
      <QuestionList {...questionListProps} />

    </div>
  );
}

Questions.propTypes = {
  metadata: PropTypes.object,
  questions: PropTypes.object,
  location: PropTypes.object,
  dispatch: PropTypes.func,
};

function mapStateToProps({ questions,app }) {
  return { questions,metadata: app.metadata };
}

export default connect(mapStateToProps)(Questions);
