import React, { PropTypes } from 'react';
import { connect } from 'dva';
import Header from '../components/layout/header';
import Bread from '../components/layout/bread';
import Footer from '../components/layout/footer';
import Sider from '../components/layout/sider';
import styles from '../components/layout/main.less';
import { Spin } from 'antd';
import { classnames } from '../utils';
import '../components/layout/common.less';

function App({ children, location, dispatch, app }) {
  const { isLogin, account, menu,metadata, siderFold, darkTheme, isNavbar, menuPopoverVisible, navOpenKeys } = app;

  const headerProps = {
    account,
    siderFold,
    location,
    isNavbar,
    menuPopoverVisible,
    navOpenKeys,
    switchMenuPopover() {
      dispatch({ type: 'app/switchMenuPopver' });
    },
    openProfile() {
      dispatch({ type: 'app/openProfile' });
    },
    logout() {
      dispatch({ type: 'app/logout' });
    },
    switchSider() {
      dispatch({ type: 'app/switchSider' });
    },
    changeOpenKeys(openKeys) {
      localStorage.setItem('navOpenKeys', JSON.stringify(openKeys));
      dispatch({ type: 'app/handleNavOpenKeys', payload: { navOpenKeys: openKeys } });
    },
  };

  const siderProps = {
    menu,
    siderFold,
    darkTheme,
    location,
    navOpenKeys,
    changeTheme() {
      dispatch({ type: 'app/changeTheme' });
    },
    changeOpenKeys(openKeys) {
      localStorage.setItem('navOpenKeys', JSON.stringify(openKeys));
      dispatch({ type: 'app/handleNavOpenKeys', payload: { navOpenKeys: openKeys } });
    },
    loadChapters() {
      dispatch({ type: 'app/loadChapters' });
    },
    loadModules(chapterId) {
      dispatch({ type: 'app/loadModules', payload: { id: chpaterId } });
    },
    loadSegments(moduleId) {
      dispatch({ type: 'app/loadSegments', payload: { id: moduleId } });
    },
  };

  if (!isLogin) { dispatch({ type: 'app/logout' }); }
  return (
    isLogin
        ? <div className={classnames(styles.layout, { [styles.fold]: isNavbar ? false : siderFold }, { [styles.withnavbar]: isNavbar })}>
          {!isNavbar ? <aside className={classnames(styles.sider, { [styles.light]: !darkTheme })}>
            <Sider {...siderProps} />
          </aside> : ''}
          <div className={styles.main}>
            <Header {...headerProps} />
            <Bread location={location} />
            <div className={styles.container}>
              <div className={styles.content}>
                {children}
              </div>
            </div>
            <Footer />
          </div>
        </div>
        : <div>no session</div>
  );
}

App.propTypes = {
  children: PropTypes.element.isRequired,
  location: PropTypes.object,
  dispatch: PropTypes.func,
  account: PropTypes.object,
  siderFold: PropTypes.bool,
  darkTheme: PropTypes.bool,
};

export default connect(({ app }) => ({ app }))(App);
