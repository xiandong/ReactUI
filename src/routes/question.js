import React, { PropTypes } from 'react';
import { Form, Button, Row, Col, Input, InputNumber, Radio, message } from 'antd';
import { routerRedux, Link } from 'dva/router';
import { connect } from 'dva';

import QuestionDetail from '../components/question/questionDetail';


function Question({ location, dispatch, questions, children }) {
  const {data,index}=questions;
  const { detail, comments, history, files,locked } = questions.question;

  const canBack=data.length!=0,canPrevious=canBack&&index>0,canNext=canBack&&index<data.length-1;

  const questionProps = { detail,
    comments,
    files,
    history,
    locked,
    navigator:{back:canBack,previous:canPrevious,next:canNext},
    answerQuestion(data, callback) {
      console.log(`question.answerQuestion()==${JSON.stringify(data)}`);
      dispatch({ type: 'questions/answerQuestion', payload: {answer:data}, onComplete: callback });
    },
    updateQuestion(data, callback) {
      console.log(`question.updateQuestion()==${JSON.stringify(data)}`);
      dispatch({ type: 'questions/updateQuestion', payload: data, onComplete: callback });
      },
    onBack(){
      dispatch({ type: 'questions/backToQuestionList',payload:{} });
    } ,
    onPrevious(){
      console.log('onPrevious');
      dispatch({ type: 'questions/openQuestion',payload:{index:index-1,id:data[index-1].id} });
    } ,
    onNext(){
      console.log('onNext');
      dispatch({ type: 'questions/openQuestion',payload:{index:index+1,id:data[index+1].id} });
    },
    onAddComment(data, callback){
      dispatch({ type: 'questions/addComment',payload:data,onComplete:callback });
    },
  };
  return (
    <div>
      {/* {JSON.stringify(question)}*/}
      <QuestionDetail {...questionProps} />
    </div>
  );
}

Question.propTypes = {
  questions: PropTypes.object,
  location: PropTypes.object,
  dispatch: PropTypes.func,
};

function mapStateToProps({ questions }) {
  return { questions };
}

export default connect(mapStateToProps)(Question);
