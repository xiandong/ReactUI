import React, { PropTypes } from 'react';
import { routerRedux } from 'dva/router';
import { connect } from 'dva';

import UserProfile from '../components/user/userProfile';

function Profile({ location, dispatch, metadata, profile, children }) {
  const { user, preferences, mode } = profile;
  const userProps = { user,
    preferences,
    metadata,
    onSave(data, callback) {
      console.log('onSave()');
      dispatch({ type: 'profile/update', payload: data, onComplete: callback });
    },
  };

  return (
    <div>
      <UserProfile {...userProps} />
    </div>
  );
}

Profile.propTypes = {
  metadata: PropTypes.object,
  profile: PropTypes.object,
  location: PropTypes.object,
  dispatch: PropTypes.func,
};

function mapStateToProps({ profile, app }) {
  return { profile, metadata: app.metadata };
}

export default connect(mapStateToProps)(Profile);
