import React, { PropTypes } from 'react';
import { routerRedux } from 'dva/router';
import { connect } from 'dva';

import Example from '../components/Example';

function ExamplePage({ location, dispatch, example, children }) {
  return (
    <div>
      <Example location={location} children={children} />

    </div>
  );
}

Example.propTypes = {
  example: PropTypes.object,
  location: PropTypes.object,
  dispatch: PropTypes.func,
};

function mapStateToProps({ example }) {
  return { example };
}

export default connect(mapStateToProps)(ExamplePage);
