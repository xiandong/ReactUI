import React, { PropTypes } from 'react';
import { connect } from 'dva';
import { Row, Col, Card } from 'antd';
import { Link } from 'dva/router';
import NumberCard from '../../components/dashboard/numberCard';
import Quote from '../../components/dashboard/quote';
import Sales from '../../components/dashboard/sales';
import Weather from '../../components/dashboard/weather';
import RecentSales from '../../components/dashboard/recentSales';
import Comments from '../../components/dashboard/comments';
import Completed from '../../components/dashboard/completed';
import Browser from '../../components/dashboard/browser';
import Cpu from '../../components/dashboard/cpu';
import User from '../../components/dashboard/user';
import styles from './dashboard.less';
import { color } from '../../utils';

const bodyStyle = {
  bodyStyle: {
    height: 432,
    background: '#fff',
  },
};

function Dashboard({ dashboard, dispatch }) {
  const { weather, sales, quote, numbers, recentSales, comments, completed, browser, cpu, user } = dashboard;
  const numberCards = numbers.map((item, key) => <Col key={key} lg={6} md={12}>
    <NumberCard {...item} />
  </Col>);

  return (
    <Row gutter={24}>
      {numberCards}
      <Col lg={12} md={24}>
        <Card
          bordered={false} bodyStyle={{
            padding: '24px 36px 24px 0',
          }}
        >
          <Sales data={sales} />
        </Card>
      </Col>
      <Col lg={12} md={24}>
        <Row>
          <Col lg={24} md={24}>
            <Card bordered >
              <ul>
                <li><Link to="/questions?chapter=1006&status=open&assignee=100">All Open Questions under my name</Link></li>
              </ul>
            </Card>
          </Col>
          <Col lg={24} md={24}>
            <Card
              bordered={false} {...bodyStyle}
              title="My questions due within one week"
            >
              {/* <Weather {...weather} />*/}
              <Comments data={comments} />
            </Card>
          </Col>
        </Row>
      </Col>
    </Row>
  );
}

Dashboard.propTypes = {
  weather: PropTypes.object,
  sales: PropTypes.array,
  quote: PropTypes.object,
  numbers: PropTypes.array,
  recentSales: PropTypes.array,
  comments: PropTypes.array,
  completed: PropTypes.array,
  browser: PropTypes.array,
  cpu: PropTypes.object,
  user: PropTypes.object,
};

export default connect(({ dashboard }) => ({ dashboard }))(Dashboard);
