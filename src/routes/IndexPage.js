import { Menu, Icon } from 'antd';
import { Link } from 'dva/router';
import { connect } from 'dva';
import styles from './IndexPage.css';

function IndexPage() {
  return (
    <div>
      <Menu
        selectedKeys={[location.pathname]}
        mode="horizontal"
        theme="dark"
      >
        <Menu.Item key="/">
          <Link to="/"><Icon type="bars" />Home</Link>
        </Menu.Item>
        <Menu.Item key="/example">
          <Link to="/example"><Icon type="home" />Example</Link>
        </Menu.Item>
        <Menu.Item key="/404">
          <Link to="/page-you-dont-know"><Icon type="frown-circle" />404</Link>
        </Menu.Item>
        <Menu.Item key="/antd">
          <a href="https://github.com/dvajs/dva">dva</a>
        </Menu.Item>
      </Menu>
      <div>This is home</div>
    </div>
  );
}

IndexPage.propTypes = {
};

export default connect()(IndexPage);
