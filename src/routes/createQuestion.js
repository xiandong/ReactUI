import React, { PropTypes } from 'react';
import { routerRedux, Link, browserHistory } from 'dva/router';
import { connect } from 'dva';
import { Form, Button, Row, Col, Input, InputNumber, Radio, message, DatePicker } from 'antd';
import { Select } from 'antd';
const Option = Select.Option;
const FormItem = Form.Item;
const ButtonGroup = Button.Group;
const { TextArea } = Input;

function CreateQuestions({
    location,
    dispatch,
    question,
    metadata,
    form: {
        getFieldDecorator,
        validateFields,
        getFieldValue,
        validateFieldsAndScroll,
    } }) {
  console.log(`question=${question}`);
  const formItemLayout = {
    labelCol: { span: 2 },
    wrapperCol: { span: 14 },
  };
  const buttonItemLayout = {
    wrapperCol: { span: 14, offset: 2 },
  };
  const onSave = (e) => {
    e.preventDefault();
    validateFieldsAndScroll((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
        values.question.organization=1;
        //values.question.content=values.question.content.formatJSON();
        dispatch({ type: 'questions/save',
          payload: values,
          onComplete: () => {
                    // redirect?
          } });
                // this.props.onSave(values, () => {
                //     this.setState({ ...this.state, mode: 'VIEW' });
                //     message.info('Save user profile successfully.');
                // });
      }
    });
  };
  const onCancel = () => {
         // dispatch({type:'questions/open'});
    browserHistory.goBack();
  };
  const handleFieldChange = (field, value) => {
    console.log(`handleFieldChange field=${field} value=${value}`);
    if (field == 'chapter') {
      question.chapterId = value;
      question.moduleId = undefined;
      // if (!metadata[`chapter_${question.chapterId}`]) {
      //   dispatch({ type: 'app/loadModules', payload: { id: value } });
      // }
    } else if (field == 'module') {
      question.moduleId = value;
      // if (!metadata[`module_${question.moduleId}`]) {
      //   dispatch({ type: 'app/loadSegments', payload: { id: value } });
      // }
    } else if (field == 'segment') {
      question.segmentId = value;
      // if (!metadata[`module_${question.moduleId}`]) {
      //   dispatch({ type: 'app/loadSegments', payload: { id: value } });
      // }
    }
  };
  const moduleList = metadata.chapters && metadata.chapters[`${question.chapterId}`] && metadata.chapters[`${question.chapterId}`].modules ?
     Object.keys(metadata.chapters[`${question.chapterId}`].modules).map((key) => {
        const module=metadata.chapters[`${question.chapterId}`].modules[key];
	    	return (<Option key={module.id}>{module.name}</Option>);
    }) : [];

  const segmentList = metadata.chapters && metadata.chapters[`${question.chapterId}`] && metadata.chapters[`${question.chapterId}`].modules
        && metadata.chapters[`${question.chapterId}`].modules[`${question.moduleId}`]
        && metadata.chapters[`${question.chapterId}`].modules[`${question.moduleId}`].segments?
     Object.keys(metadata.chapters[`${question.chapterId}`].modules[`${question.moduleId}`].segments).map((key) => {
        const segment=metadata.chapters[`${question.chapterId}`].modules[`${question.moduleId}`].segments[key];
	    	return (<Option key={segment.id}>{segment.name}</Option>);
    }) : [];
  return (
    <Form horizontal>
      <FormItem label="Chapter" {...formItemLayout}>
        {
                    getFieldDecorator('question.chapter', {
                      rules: [
                        {
                          required: true,
                          message: 'The chapter is required.',
                        },
                      ],
                    })(
                      <Select
                        style={{ width: 200 }}
                        placeholder="Select a chapter"
                        onChange={(...arg) => {
                          handleFieldChange('chapter', ...arg);
                        }
                           }
                      >
                        {
                                metadata.chapters ? Object.keys(metadata.chapters).map((key) => {
                                  console.log('key='+key+'metadata=='+JSON.stringify(metadata.chapters[key]));
                                  const chapter=metadata.chapters[key];
                                  return (<Option key={chapter.id}>{chapter.title}</Option>);
                                }) : undefined
                            }


                      </Select>,
                        )
                }
      </FormItem>

      <FormItem label="Module" {...formItemLayout}>
        {
                    getFieldDecorator('question.module', {
                      rules: [
                        {
                          required: true,
                          message: 'The module is required.',
                        },
                      ],
                    })(
                      <Select
                        style={{ width: 200 }}
                        placeholder="Select a module"
                        onChange={(...arg) => {
                          handleFieldChange('module', ...arg);
                        }
                           }
                      >

                               { moduleList}

                      </Select>,
                        )
                }
      </FormItem>
      <FormItem label="Segment" {...formItemLayout}>
        {
                    getFieldDecorator('question.segment', {
                      rules: [
                        {
                          required: true,
                          message: 'The segment is required.',
                        },
                      ],
                    })(
                      <Select
                        style={{ width: 200 }}
                        placeholder="Select a segment"
                        optionFilterProp="children"
                        filterOption={(input, option) => option.props.value.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                      >
                        {
                                segmentList
                            }


                      </Select>,
                        )
                }
      </FormItem>
      <FormItem label="Name" {...formItemLayout}>
        {
                    getFieldDecorator('question.name', {
                      rules: [
                        {
                          required: true,
                          message: 'The name is required.',
                        },
                      ],
                    })(<Input type="tex"  />)
                }
      </FormItem>
      <FormItem label="Content" {...formItemLayout}>
        {
                    getFieldDecorator('question.content', {
                      rules: [
                        {
                          required: true,
                          message: 'The content is required.',
                        },
                      ],
                    })(<TextArea rows={8} />)
                }
      </FormItem>
      <FormItem label="Assignee" {...formItemLayout}>
        {
                    getFieldDecorator('question.assignee', {
                      rules: [
                        {
                          required: true,
                          message: 'The assignee is required.',
                        },
                      ],
                    })(
                      <Select
                        style={{ width: 200 }}
                        placeholder="Select a assignee"
                        optionFilterProp="children"
                        filterOption={(input, option) => option.props.value.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                      >
                        <Option value="1">Admin</Option>

                      </Select>,
                        )
                }
      </FormItem>

      {/* <FormItem label="Due Date" {...formItemLayout}>
        {
                    getFieldDecorator('question.dueDate', {
                      rules: [
                        {
                          required: true,
                          message: 'The due date is required.',
                        },
                      ],
                    })(
                      <DatePicker size="default" />,
                        )
                }
      </FormItem> */}
      <FormItem {...buttonItemLayout}>
        <Row>
          <Col span={4}>
            <Button type="primary" size="large" onClick={onSave}>Submit</Button>
          </Col>
          <Col span={4}>
            <Button type="primary" size="large" onClick={onCancel}>Cancel</Button>
          </Col>
        </Row>
      </FormItem>
    </Form>
  );
}

CreateQuestions.propTypes = {
  questions: PropTypes.object,
  metadata: PropTypes.object,
  location: PropTypes.object,
  dispatch: PropTypes.func,
};

function mapStateToProps({ questions, app }) {
  return { question: questions.question, metadata: app.metadata };
}

export default connect(mapStateToProps)(Form.create()(CreateQuestions));
