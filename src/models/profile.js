

import {
    fetchUserDetail,
    fetchUserPreferences,
    saveUser,
    changPassword,
} from '../services/user';
import { storageTokenKey } from '../utils/constants';

import { routerRedux } from 'dva/router';
import { message } from 'antd';
import pathToRegExp from 'path-to-regexp';
export default {

  namespace: 'profile',

  state: {
    user: {},
    preferences: {},
  },

  subscriptions: {
    setup({ dispatch, history }) {  // eslint-disable-line
     // called when app up
      history.listen((location) => {
        // refer https://github.com/pillarjs/path-to-regexp
        // let keys=[];
        // if (pathToRegExp('/profile/:userId',keys).exec(location.pathname)) {
        //     console.log('keys='+keys);
        //     dispatch({type: 'fetch', payload: {userId}})
        // }
 console.log('profile model setup()');
        if (pathToRegExp('/profile').exec(location.pathname)) {
          dispatch({ type: 'fetch' });
        }
      });
    },
  },

  effects: {
    *fetch({ payload }, { select,call, put }) {  // eslint-disable-line
      // console.log('profile.fetch()....');
      //   console.log('profile.fetch() finsih....');
      // const userId = yield select(({ app }) => {
      //   return app.account.user_id}
      // )
      // the app state is not populated now
      yield [put({ type: 'fetchUserDetail' }), put({ type: 'fetchUserPreferences' })];
    },
    *update({ payload,onComplete }, { call, put }) {  // eslint-disable-line
      const { data } = yield call(saveUser, payload);
      if (data) {
        yield [put({ type: 'saveUser', payload: { user: data.user } }), put({ type: 'savePreferences', payload: { preferences: data.preferences } })];
        onComplete();
      }
    },

    * fetchUserDetail({ payload }, { put, call }) {
      const { data } = yield call(fetchUserDetail);
      if (data) {
        yield put({
          type: 'saveUser',
          payload: { user: data },
        });
      }
    },
    * fetchUserPreferences({ payload }, { put, call }) {
      const { data } = yield call(fetchUserPreferences);
      if (data) {
        yield put({
          type: 'savePreferences',
          payload: { preferences: data },
        });
      }
    },
  },

  reducers: {
    saveUser(state, { payload }) {
      const { user } = payload;
      console.log(`saveUser user==${JSON.stringify(user)}`);
      return { ...state, user };
    },
    savePreferences(state, { payload }) {
      const { preferences } = payload;
      return { ...state, preferences };
    },
    switchToEdit(state, { payload }) {
      return { ...state, mode: 'EDIT' };
    },
    saveProfile(state, { payload }) {
      return { ...state, mode: 'VIEW' };
    },
  },

};
