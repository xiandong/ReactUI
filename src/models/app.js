import { routerRedux } from 'dva/router';
import { message } from 'antd';
import { auth, loadMenu, loadMetadata, getChapters, getModules, getSegments } from '../services/app';
import { createUser, fetchUser } from '../services/user';
import { storageTokenKey } from '../utils/constants';


export default {
  namespace: 'app',
  state: {
    isLogin: false,
    account: {
      username: null,
      ability: null,
      user_id: null,
      email: null,
    },
    menuPopoverVisible: false,
    menu: [], // menu in sidebar
    metadata: {}, // cache all list value,chapter,modules,segment
    siderFold: localStorage.getItem('antdAdminSiderFold') === 'true',
    darkTheme: localStorage.getItem('antdAdminDarkTheme') !== 'false',
    isNavbar: document.body.clientWidth < 769,
    navOpenKeys: JSON.parse(localStorage.getItem('navOpenKeys') || '[]'), // 侧边栏菜单打开的keys
  },
  subscriptions: {
    setup({ dispatch }, error) {
      console.log('dashboard.setup');
      window.onresize = function () {
        dispatch({ type: 'changeNavbar' });
      };
    },
  },
  effects: {
    * auth({ payload }, { call, put }) {
      const { username, password } = payload;
      try {
        const { data } = yield call(auth, { username, password });

                // succeed to login
        if (data) {
          const { access_token, token_type } = data;
          const token = access_token;
                    // save the token to the local storage.
          window.localStorage.setItem(storageTokenKey, token);
          yield put({
            type: 'authSuccess',
          });
          yield put(routerRedux.push('/'));
        }
      } catch (error) {
                // console.log('error='+error.message);
        message.error('Invalid user or password.', 10);
      }
    },
    * enterAuth({ payload, onComplete }, { put, take }) {
      yield [put({ type: 'checkToken' }), put({ type: 'queryUser' }), put({ type: 'loadMenu' }), put({ type: 'loadMetadata' }),
        put({ type: 'loadChapters' })];

            // what is take?
            // yield [take('app/hasToken'), take('app/queryUserSuccess')];
            // requried when the effect is triggered from onEnter() which is blocking call.
            // react router document - https://github.com/ReactTraining/react-router/blob/master/docs/API.md
            // If callback is listed as a 3rd argument, this hook will run asynchronously, and the transition will block until callback is called.
      onComplete();// render the page. Add other call below after page render.
    },
    * checkToken({ payload }, { put, call, select }) {
            // get the token from local storage.
      console.log(`checkToken storageTokenKey=${storageTokenKey}`);
      const token = window.localStorage.getItem(storageTokenKey);
      console.log(`checkToken token=${token}`);
      if (token) {
        yield put({ type: 'hasToken' });
      } else {
        yield put({ type: 'authFail' });
      }
    },
    * openProfile({ payload }, { put }) {
      yield put(routerRedux.push('/profile'));
    },
    * logout({ payload }, { put }) {
      yield put({ type: 'authFail' });
      window.localStorage.removeItem(storageTokenKey);
      yield put(routerRedux.push('/login'));
    },
    * queryUser({ payload }, { put, call }) {
      const { data } = yield call(fetchUser);
      if (data) {
        yield put({
          type: 'queryUserSuccess',
          payload: { account: data },
        });
      }
    },
    * register({ payload }, { put, call }) {
      const { username, email, password } = payload;
      const { data } = yield call(createUser, { username, email, password });
      if (data) {
        yield put({
          type: 'auth',
          payload: { username, password },
        });
      }
    },
    * loadMenu({ payload }, { put, call }) {
      console.log('before load menu');
      const { data } = yield call(loadMenu);
      console.log('after load menu +'+data);
      if (data) {
        console.log(`load menu succeed ${JSON.stringify(data)}`);
        yield put({
          type: 'loadMenuSuccess',
          payload: { menu: data },
        });
      }
    },
    * loadChapters({ payload }, { put, call }) {
      const { data } = yield call(getChapters);
      if (data) {
        console.log(`load chapters succeed ${JSON.stringify(data)}`);
        yield put({
          type: 'loadChapterSuccess',
          payload: { chapters: data },
        });

        let menu=[];
        data.map(chapter=>{
          menu.push({id:chapter.id,key:chapter.id,name:chapter.title,child:[]});
        });
       //patch menu
       yield put({
          type: 'patchChapterMenu',
          payload: {menu:menu},
        });

        //load modules
        for (let chapter of data){
           yield put({ type: 'loadModules', payload: { id: chapter.id } });
        };
      }
    },
    * loadModules({ payload }, { put, call }) {
      const { id } = payload;
      console.log(`loadModules id=${id}`);
      const { data } = yield call(getModules, { chapterId: id });
      if (data) {
        console.log(`load getModules succeed ${JSON.stringify(data)}`);
        yield put({
          type: 'loadModuleSuccess',
          payload: { chapterId: id, modules: data },
        });
        //patch menu
        let menu=[];
        data.map(module=>{
          menu.push({key:module.id,name:module.name,id:module.id,link:`questions?chapter=${id}&module=${module.id}`});
        });
        yield put({
          type: 'patchModuleMenu',
          payload: {chapterId:id,menu:menu},
        });

        //load segments
        //https://stackoverflow.com/questions/33316765/why-is-es6-yield-a-reserved-word-when-called-in-this-context
        // data.map(module => {
        //   yield put({ type: 'loadSegments', payload: {  chapterId: id,moduleId:module.id } });
        // });
        for (let module of data){
           yield put({ type: 'loadSegments', payload: {  chapterId: id,moduleId:module.id  } });
        };

      }
    },
    * loadSegments({ payload }, { put, call }) {
      const { chapterId, moduleId } = payload;
      const { data } = yield call(getSegments, { chapterId:chapterId,moduleId: moduleId });
      if (data) {
        console.log(`load getSegments succeed ${JSON.stringify(data)}`);
        yield put({
          type: 'loadSegmentSuccess',
          payload: { chapterId:chapterId,moduleId: moduleId, segments: data },
        });
      }
    },
    * loadMetadata({ payload }, { put, call }) {
      const { data } = yield call(loadMetadata);
      if (data) {
        console.log(`load metadata succeed ${JSON.stringify(data)}`);
        yield put({
          type: 'loadMetadataSuccess',
          payload: { data },
        });
      }
    },
    * switchSider({
          payload,
        }, { put }) {
      yield put({
        type: 'handleSwitchSider',
      });
    },
    * changeTheme({
          payload,
        }, { put }) {
      yield put({
        type: 'handleChangeTheme',
      });
    },
    * changeNavbar({
          payload,
        }, { put }) {
      if (document.body.clientWidth < 769) {
        yield put({ type: 'showNavbar' });
      } else {
        yield put({ type: 'hideNavbar' });
      }
    },
    * switchMenuPopver({
          payload,
        }, { put }) {
      yield put({
        type: 'handleSwitchMenuPopver',
      });
    },
  },
  reducers: {
    authSuccess(state) {
      return {
        ...state,
        isLogin: true,
      };
    },
    hasToken(state) {
      return {
        ...state,
        isLogin: true,
      };
    },
    queryUserSuccess(state, { payload }) {
      const { account } = payload;
      console.log(`queryUserSuccess payload=${JSON.stringify(payload)}`);
      return {
        ...state,
        account,
      };
    },
    loadMenuSuccess(state, { payload }) {
      const { menu } = payload;
      return {
        ...state,
        menu,
      };
    },
    patchChapterMenu(state, { payload }) {
      console.log('patchQuestionMenu');
      const { menu:chapterMenu } = payload;
      let stateMenu = state.menu;
      stateMenu.map(menu=>{
        if(menu.key=='questionaire'){
          menu.child=chapterMenu;
        }
      })
      return {...state,menu:stateMenu}
    },
    patchModuleMenu(state, { payload }) {
      console.log('patchQuestionMenu');
      const {chapterId,menu:modules}=payload;
      let stateMenu = state.menu;
      stateMenu.map(menu=>{
        if(menu.key=='questionaire'){
          menu.child.map(chapter=>{
            if(chapter.id==chapterId){
              chapter.child=modules;
            }
          })
        }
      })
      return {...state,menu:stateMenu}
    },

    loadChapterSuccess(state, { payload }) {
      const { chapters } = payload;
            // create new instance
      const metadata = { ...state.metadata };
      metadata.chapters ={};
      chapters.map(chapter=>{
        metadata.chapters[chapter.id]=chapter;
      });
      return {
        ...state,
        metadata,
      };
    },
    loadModuleSuccess(state, { payload }) {
      const { chapterId, modules } = payload;
      const metadata = { ...state.metadata };
      metadata.chapters[chapterId].modules ={};
      modules.map(module=> metadata.chapters[chapterId].modules[module.id]=module);
      return {
        ...state,
        metadata,
      };
    },
    loadSegmentSuccess(state, { payload }) {
      const { chapterId,moduleId, segments } = payload;
      const metadata = { ...state.metadata };
      metadata.chapters[chapterId].modules[moduleId].segments ={};
      segments.map(segment=> metadata.chapters[chapterId].modules[moduleId].segments[segment.id]=segment);
      return {
        ...state,
        metadata,
      };
    },
    loadMetadataSuccess(state, { payload }) {
      const { data } = payload;
      console.log(`state=${state}`);
      const metadata = { ...state.metadata };
      Object.keys(data).forEach((k) => {
        metadata[k] = data[k];
      });

      return {
        ...state,
        metadata,
      };
    },
    authFail(state) {
      return {
        ...state,
        isLogin: false,
        account: {
          username: null,
          ability: null,
          user_id: null,
          email: null,
        },
      };
    },
    handleSwitchSider(state) {
      localStorage.setItem('antdAdminSiderFold', !state.siderFold);
      return {
        ...state,
        siderFold: !state.siderFold,
      };
    },
    handleChangeTheme(state) {
      localStorage.setItem('antdAdminDarkTheme', !state.darkTheme);
      return {
        ...state,
        darkTheme: !state.darkTheme,
      };
    },
    showNavbar(state) {
      return {
        ...state,
        isNavbar: true,
      };
    },
    hideNavbar(state) {
      return {
        ...state,
        isNavbar: false,
      };
    },
    handleSwitchMenuPopver(state) {
      return {
        ...state,
        menuPopoverVisible: !state.menuPopoverVisible,
      };
    },
    handleNavOpenKeys(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
  },

};
