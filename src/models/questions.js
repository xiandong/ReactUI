import {routerRedux} from 'dva/router';
import pathToRegExp from 'path-to-regexp';
import moment from 'moment';

import {createQuestion, updateQuestion,fetchQuestionsByModule,
  fetchQuestions,fetchQuestion,fetchAnswer,updateAnswer,
  fetchComments,addComment} from '../services/question';

export default {
  // question list page- search result?
  namespace : 'questions',
  state : {
    // which capter the list belongs to. FIXME need? chapter:{}, module:{},//need?
    // segment:{},//need?
    data: [], // list question
    question: {
      locked:false,
      detail: {},
      comments: [],
      history: [],
      files: []
    }, // create/open question
    index:0,
    // {name:'chapter',label:'Chapter',value:'1',resolvedValue:'Forced Label'},{name:'status',label:'Status',value:'Open',resolvedValue:'Open'},{name:'assignee',label:'Assignee',value:'100',resolvedValue:'Jack Ma'}
    criteria: {}, // include criteria entry to build the filterBox
    // use table column filterplus the text box. no select option.
    filter: {
      keyword: ''
    }, // query to filter
  },

  subscriptions : {
    setup({dispatch, history}) { // eslint-disable-line
      console.log('questions model setup()');
      history.listen((location) => {
        console.log('dashboard.setup history listen');
        if (pathToRegExp('/questions').exec(location.pathname)) {
          // criteria FIXME
          const criteria = location.query;
          // FIXME fetch with criteria
          dispatch({type: 'fetch',payload: criteria});
          //dispatch({type: 'buildCriteria', payload: {criteria}});
          dispatch({type: 'updateCriteria', payload: criteria});
        }
      });
    }
  },

  effects : {
    //MOVE TO PAGE DUE TO METADATA IS NOT READY WHEN REFRESH PAGE
    // * buildCriteria({payload}, {call, put,select}){
    //       const {criteria}=payload;
    //       //the metadata may be not ready since this method is called by subscriptions.
    //       const metadata=yield select(state => state.app.metadata);
    //       // resolve label field
    //       let criteriaRows = [];
    //       Object
    //         .keys(criteria)
    //         .forEach((k) => {
    //           // FIXME resolve label and resolvedValue
    //           const label = k;
    //           let resolvedValue = criteria[k];
    //           if(k=='chapter'){
    //             resolvedValue=metadata.chapters[criteria['chapter']].value;
    //           }
    //           if(k=='module'){
    //             if(criteria['chapter'])
    //               resolvedValue=metadata.chapters[criteria['chapter']].modules[criteria['module']].value;
    //           }
    //           criteriaRows.push({name: k, label, value: criteria[k], resolvedValue});
    //         });
    //       yield put({type: 'updateCriteria', payload: criteriaRows});
    // },
    * fetch({
      payload
    }, {call, put}) { // eslint-disable-line
      yield put({type: 'clearQuestions'});
      const {data} = yield call(fetchQuestions, payload);
      if (data) {
        console.log(`fetch questions==${JSON.stringify(data)}`);
        const questions=data.map(row=>{
          return {id:row.id,name:row.name,content:row.content.text,locked:row.locked};
        })
        // yield put({type: 'updateSegment', payload: {     id   }});
        yield put({type: 'updateQuestions', payload: {
            data:questions
          }});
      }
    },
    * create({
      payload
    }, {call, put}) {
      yield put(routerRedux.push('/questions/createnew'));
    },
    * save({
      payload
    }, {call, put, select}) {
      // add parent const id=yield select(state => state.questions.segment.id);
      const {question} = payload;
      const {data} = yield call(createQuestion, {
        ...question
      });
      if (data) {
        console.log('create question succeed');
        // browserHistory.goBack();
        const {id} = data;
        yield put(routerRedux.push(`/questions/${id}`));
      }
    },
     * updateQuestion({
      payload,onComplete
    }, {call, put, select}) {
      // add parent const id=yield select(state => state.questions.segment.id);
      const {id} = yield select(state => state.questions.question.detail);
      const {detail} = payload;
      detail.id=id;
      const {data} = yield call(updateQuestion, {
        ...detail
      });
      if (data) {
        console.log('update question succeed');
        // browserHistory.goBack();
        const {id} = data;
        yield put({
          type:'getQuestion',
          payload:{
           id
          }
        });
        onComplete();
        //yield put(routerRedux.push(`/questions/${id}`));
      }
    },
    // open question list page FIXME url with passing criteria
    * open({
      payload
    }, {call, put, select}) {
      const id = yield select(state => state.questions.segment.id);
      yield put(routerRedux.push(`/questions?id=${id}`));
    },
    * openQuestion({
      payload
    }, {call, put, select}) {
      const {id,index} = payload;
      yield put({
          type: 'updateIndex',
          payload: { index },
        });
      yield put(routerRedux.push(`/questions/${id}`));
    },
    * backToQuestionList({payload},{put,select}){
      console.log('backToQuestionlist');
      //?chapter=2&module=1
      let criteriaUrl='?';
      const criteria = yield select(state => state.questions.criteria);
      Object.keys(criteria).map(key=>{
        criteriaUrl+=(key+'='+criteria[key]+'&');
      })
      criteriaUrl=criteriaUrl.slice(0,-1);
      yield put(routerRedux.push(`/questions${criteriaUrl}`));
    },
    *getQuestion({ payload }, { call, put }) {  // eslint-disable-line
      // avoid asyc fetch delay.
      yield put({
        type: 'clearQuestion',
        payload: { detail: {} },
      });
      const { data:question } = yield call(fetchQuestion, payload);
      const { data:answer } = yield call(fetchAnswer, payload);
      if (question && answer) {
        console.log(`fetch question==${JSON.stringify(question)}`);
        console.log(`fetch answer==${JSON.stringify(answer)}`);

        let detail={};
        detail.id=question.id;
        detail.name=question.name;
        detail.content=question.content.text;
        detail.answer=answer.content;
        detail.locked=question.locked;
        detail.creator= question.systemFields.created_by,
				detail.createDate= question.systemFields.created_at;
        yield put({
          type: 'saveQuestion',
          payload: { detail: detail },
        });

        yield put({
          type:'getComments',
          payload
        })
      }
    },
    *answerQuestion({
      payload
    }, {call, put, select}) {
      // add parent const id=yield select(state => state.questions.segment.id);
      const detail = yield select(state => state.questions.question.detail);
      const {answer} = payload;
      const param={id:detail.id,answer:answer,correctness:true};
      const {data} = yield call(updateAnswer, {
        ...param
      });
      if (data) {
        console.log('answer question succeed');
        // browserHistory.goBack();
        const {question:id} = data;
        yield put({
          type:'getQuestion',
          payload:{
           id
          }
        });
        yield put(routerRedux.push(`/questions/${id}`));
      }
    },
    *getComments({ payload }, { call, put }){
      const {data} = yield call(fetchComments, payload);
      if (data) {
        yield put({
          type:'updateComments',
          payload:{
           comments:data
          }
        });
      }
    },
    *addComment({payload,onComplete}, {call, put, select}){
      const {id} = yield select(state => state.questions.question.detail);
      const {value} = payload;
      const data={id,comment:value};
       yield call(addComment, {
        ...data
      });
      yield put({
        type:'getComments',
        payload:{id}
      })
      onComplete();
    },
    *getHistory(){

    }
  },

  reducers : {
    clearQuestions(state, {payload}) {
      const data = [];
      return {
        ...state,
        data
      };
    },
    updateCriteria(state, {payload}) {
      const criteria = payload;
      return {
        ...state,
        ...{criteria}
      };
    },
    updateQuestions(state, {payload}) {
      let {data} = payload;
      // patch data with missing data
      data = data.map((row) => {
        const pRow = {
          assignee: 100,
          dueDate: moment(),
          // answer: {
          //   product: true
          // }
        };
        //row.content = row.attributes.content;
        return {
          ...row,
          ...pRow
        };
      });
      console.log(`updateQuestions questions=${data}`);
      return {
        ...state,
        data
      };
    },
    clearQuestion(state, { payload }) {
      console.log(`saveQuestion question detail==${JSON.stringify(detail)}`);
      // state.detail=detail;
      // return state;
      const detail = {};
      return { ...state, detail };
    },
    saveQuestion(state, { payload }) {
      const { detail } = payload;
      const {locked}=payload.detail;
      console.log(`saveQuestion question detail==${JSON.stringify(detail)}`);
      const question={locked,detail};
      // state.detail=detail;
      // return state;
      return { ...state, question};
    },
    saveAnswer(state, { payload }) {
      const { question } = payload;
      const answer = question;
      console.log(`saveQuestion question detail==${JSON.stringify(answer)}`);
      const detail = state.detail;
      detail.answer = answer;
      return { ...state, detail };
    },
    updateIndex(state,{payload}){
      const {index}=payload;
      return {...state,index}
    },
    updateComments(state,{payload}) {
      const {question}=state;
      question.comments=payload.comments;
      return { ...state, question};
    }

  }
};
