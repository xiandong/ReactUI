import React from 'react';
import { Router, Route, IndexRedirect } from 'dva/router';
import Login from './routes/login/login';
import App from './routes/app';
import Dashboard from './routes/dashboard/dashboard';
import Profile from './routes/profile';
import Questions from './routes/questions';
import CreateQuestion from './routes/createQuestion';
import Question from './routes/question';

import ExamplePage from './routes/examplePage';

function RouterConfig({ history, app }) {
  function requireAuth(nextState, replace, callback) {
    console.log('requireAuth...');
    app._store.dispatch({
      type: 'app/enterAuth',
      payload: {},
      onComplete: callback, // enforce auth is blocking call
    });
  }

  function enterProfile(nextState, replace, callback) {
    console.log('enterProfile...');
    // app._store.dispatch({
    //   type: 'app/enterAuth',
    //   payload: {},
    //   onComplete: callback, // enforce auth is blocking call
    // });
  }

  function prepareQuestion(nextState) {
    app._store.dispatch({
      type: 'questions/getQuestion',
      payload: { id: nextState.params.id },
    });
  }

  return (
    <Router history={history}>
      <Route path="/login" component={Login} />
      <Route
        path="/"
        breadcrumbName="Home"
        icon="file-text"
        component={App}
        onEnter={requireAuth}
      >
        <IndexRedirect to="dashboard" />
        <Route
          path="dashboard"
          breadcrumbName="Dashboard"
          icon="file-text"
          component={Dashboard}
        />
        <Route
          path="profile"
          breadcrumbName="Profile"
          icon="file-text"
          //onEnter={enterProfile}
          component={Profile}
        />
        <Route
          path="questions"
          breadcrumbName="Questions"
          icon="file-text"
          component={Questions}
        />
        <Route
          path="questions/createnew"
          breadcrumbName="Questions"
          icon="file-text"
          component={CreateQuestion}
        />
        <Route
          path="questions/:id"
          breadcrumbName="Question"
          icon="file-text"
          component={Question}
          onEnter={prepareQuestion}
        />
        <Route
          path="*"
          breadcrumbName="Not Found" component={props => <h1>Oops! Not Found</h1>}
        />

      </Route>
      <Route
        path="example"
        breadcrumbName="Example"
        icon="file-text"
        component={ExamplePage}
      >

      </Route>
      <Route path="*" breadcrumbName="Not Found" component={props => <h1>Oops! Not Found</h1>} />
    </Router>
  );
}

export default RouterConfig;
