module.exports = [
  {
    key: 'dashboard',
    name: 'Dashboard',
    icon: 'laptop',
  },
  {
    key: 'users',
    name: 'User Management',
    icon: 'user',
  },
  {
    key: 'questions',
    name: 'Questions',
    icon: 'camera-o',
    clickable: true,
    child: [
      {
        key: 'question',
        name: 'Question',
      },
    ],
  },
  {
    key: 'question',
    name: 'Question',
    icon: 'user',
  },
];
