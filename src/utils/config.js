module.exports = {
  name: 'Simpli Compliance',
  prefix: 'antdAdmin',
  footerText: 'Simpli Compliance',
  logoSrc: 'https://t.alipayobjects.com/images/rmsweb/T1B9hfXcdvXXXXXXXX.svg',
  logoText: 'Simpli Compliance',
  needLogin: true,
};
