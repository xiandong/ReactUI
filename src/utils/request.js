import fetch from 'dva/fetch';
const Ajax = require('robe-ajax');
import {storageTokenKey} from '../utils/constants';


function parseJSON(response) {
  return response.json();
}

function checkStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return response;
  }

  const error = new Error(response.statusText);
  error.response = response;
  throw error;
}

/**
 * Requests a URL, returning a promise.
 *
 * @param  {string} url       The URL we want to request
 * @param  {object} [options] The options we want to pass to "fetch"
 * @return {object}           An object containing either "data" or "err"
 */
export default function request(url, options) {
  console.log(`request ===> url=${url} options=${JSON.stringify(options)}`);
  if (options.cross) {
    return Ajax.getJSON('http://query.yahooapis.com/v1/public/yql', {
      q: `select * from json where url='${url}?${Ajax.param(options.data)}'`,
      format: 'json',
    });
  }
  return fetch(url, options)
      .then(checkStatus)
      .then(parseJSON)
      .then(data => ({ data }))
      .catch((err) => {
        //instead of error out, return empty for 404 due to server treat no data found as 404.
        if(err.response && err.response.status==404)
          return {data:{}};
        throw err;
      });
}

export function upload(url,file){
  const token = window.localStorage.getItem(storageTokenKey);
  return new Promise(
    (resolve, reject) => {
      const xhr = new XMLHttpRequest();
      xhr.open('POST', url);
      xhr.setRequestHeader('Authorization', `Bearer ${token}`);
      const data = new FormData();
      data.append('image', file);
      xhr.send(data);
      xhr.addEventListener('load', () => {
        const response = JSON.parse(xhr.responseText);
        resolve(response);
      });
      xhr.addEventListener('error', () => {
        const error = JSON.parse(xhr.responseText);
        reject(error);
      });
    }
  );
}
